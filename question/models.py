from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')
    birthday = models.DateField(verbose_name='Дата рождения')

    class Meta:
        verbose_name='Автор'
        verbose_name_plural='Авторы'


class Article(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    date_published = models.DateTimeField(verbose_name='Дата публикации')
    is_published = models.BooleanField(verbose_name='Опубликовано')
    text = models.TextField(verbose_name='Текст')
    likes = models.IntegerField(verbose_name='Лайки', default=0)
    picture = models.ImageField(upload_to='uploads/%Y/%m/%d/', null=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    class Meta:
        verbose_name='Статья'
        verbose_name_plural='Статьи'
