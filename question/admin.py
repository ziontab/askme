from django.contrib import admin
from question.models import Author, Article

admin.site.register(Author)
admin.site.register(Article)
