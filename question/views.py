import json
import requests

from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST

from question.forms import AuthorForm, ArticleForm
from question import models


def index(request):
    questions_list = [
        {"name": "my best questiion", "id": 1},
        {"name": "my best questiion2", "id": 2},
    ]
    return render(request, "question/index.html", {
        "questions": questions_list,
    })


def question(request, id):
    return render(request, "question/question.html", {})


def authors(request):
    authors_list = models.Author.objects.all()
    return render(request, "question/authors.html", {"authors": authors_list})


def add_author(request):
    form = AuthorForm()
    if request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('authors')
    return render(request, "question/add_form.html", {"form": form})


def articles(request):
    articles_list = models.Article.objects.all()
    return render(request, "question/articles.html", {"articles": articles_list})


def add_article(request):
    form = ArticleForm()
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            article = form.save()
            send_message(article)
            return redirect('articles')
    return render(request, "question/add_form.html", {"form": form})


@require_POST
def like_article(request):
    article_id = request.POST.get('article_id', '')
    article = models.Article.objects.filter(id=article_id).first()
    if not article:
        return JsonResponse({"status": "error"})

    article.likes += 1
    article.save()

    return JsonResponse({"status": "ok"})


def send_message(article):
    command = {
        "method": "publish",
        "params": {
            "channel": "new_posts",
            "data": {
                "article": article.id,
                "article_text": article.text,
            }
        }
    }
    headers = {
        'Content-type': 'application/json',
        'Authorization': 'apikey ' + settings.CENTRIFUGO_KEY
    }
    requests.post(
        "http://{}/api".format(settings.CENTRIFUGO_HOST),
        data=json.dumps(command),
        headers=headers
    )
